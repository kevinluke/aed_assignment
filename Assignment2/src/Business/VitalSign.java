/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;



/**
 *
 * @author 健行
 */
public class VitalSign {

    private float resrate;
    private float heartrate;
    private float bloodrate;
    private float weight;
    private String dateStr;
    private String status;

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    


    public float getResrate() {
        return resrate;
    }

    public void setResrate(float resrate) {
        this.resrate = resrate;
    }

    public float getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(float heartrate) {
        this.heartrate = heartrate;
    }

    public float getBloodrate() {
        return bloodrate;
    }

    public void setBloodrate(float bloodrate) {
        this.bloodrate = bloodrate;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
    @Override
    public String toString(){
        return this.dateStr;
    }
         
}
