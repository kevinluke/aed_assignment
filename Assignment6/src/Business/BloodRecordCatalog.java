/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class BloodRecordCatalog {
        private ArrayList<BloodRecord> bloodRecordList;
    
    public BloodRecordCatalog(){
        this.bloodRecordList = new ArrayList<>();
    }
    
    public BloodRecord addBloodRecord(){
       
        BloodRecord br = new BloodRecord();
        bloodRecordList.add(br);
        return br;
    }
        public void deleteBloodRecord(BloodRecord br){
        bloodRecordList.remove(br);
    }
    
    public ArrayList<BloodRecord> getBloodRecordList(){
        return bloodRecordList;
    }
    
    
        public BloodRecord searchBloodRecord(String keyWord){
            for (BloodRecord br: bloodRecordList){
                if (br.getDonorName().equals(keyWord)){
                    return br;
        }
    }
        return null;
    }
    @Override
    public String toString(){
        return "BloodRecord";
    }
}
