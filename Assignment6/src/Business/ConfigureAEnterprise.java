/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Enterprise.HospitalEnterprise;
import Business.Organization.AdminOrganization;
import Business.Organization.DonorOrganization;
import Business.Organization.NurseOrganization;
import Business.Organization.ReceptionistOrganization;
import Business.Role.AdminRole;
import Business.Role.DonorRole;
import Business.Role.NurseRole;
import Business.Role.ReceptionistRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author 健行
 */
public class ConfigureAEnterprise {
    public static Enterprise configure(){

        Enterprise enterprise = new HospitalEnterprise("hospital");
        AdminOrganization adminOrganization = new AdminOrganization();
        DonorOrganization donorOrganization = new DonorOrganization();
        NurseOrganization nurseOrganization = new NurseOrganization();
        ReceptionistOrganization receptionistOrganization = new ReceptionistOrganization();
        enterprise.getOrganizationDirectory().getOrganizationList().add(adminOrganization);
        enterprise.getOrganizationDirectory().getOrganizationList().add(donorOrganization);
        enterprise.getOrganizationDirectory().getOrganizationList().add(nurseOrganization);
        enterprise.getOrganizationDirectory().getOrganizationList().add(receptionistOrganization);
        
        Employee employee1 = new Employee();
        employee1.setName("Raunak Agarwal");
        
        UserAccount account1 = new UserAccount();
        account1.setUsername("admin");
        account1.setPassword("admin");
        account1.setRole(new AdminRole());
        account1.setEmployee(employee1);
        
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee1);
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account1);
        
        Employee employee2 = new Employee();
        employee2.setName("donor1");
        
        UserAccount account2 = new UserAccount();
        account2.setUsername("donor");
        account2.setPassword("donor");
        account2.setRole(new DonorRole());
        account2.setEmployee(employee2);
        
        donorOrganization.getEmployeeDirectory().getEmployeeList().add(employee2);
        donorOrganization.getUserAccountDirectory().getUserAccountList().add(account2);
        
        Employee employee3 = new Employee();
        employee3.setName("nurse1");
        
        UserAccount account3 = new UserAccount();
        account3.setUsername("nurse");
        account3.setPassword("nurse");
        account3.setRole(new NurseRole());
        account3.setEmployee(employee3);
        
        nurseOrganization.getEmployeeDirectory().getEmployeeList().add(employee3);
        nurseOrganization.getUserAccountDirectory().getUserAccountList().add(account3);
        
        Employee employee4 = new Employee();
        employee4.setName("receptionist");
        
        UserAccount account4 = new UserAccount();
        account4.setUsername("receptionist");
        account4.setPassword("receptionist");
        account4.setRole(new ReceptionistRole());
        account4.setEmployee(employee4);
        
        receptionistOrganization.getEmployeeDirectory().getEmployeeList().add(employee4);
        receptionistOrganization.getUserAccountDirectory().getUserAccountList().add(account4);
        
        
        
        return enterprise;
}
}