/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;


import java.awt.BorderLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
        import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
/**
 *
 * @author 健行
 */


public class Showgraph extends ApplicationFrame {
private Person p;
 public Showgraph(String s, Person person) {
  super(s);
  p = person;
  setContentPane(createDemoLine());
 }


 
 

 // 生成显示图表的面板
public JPanel createDemoLine() {
  JFreeChart jfreechart = createChart(createDataset());
  return new ChartPanel(jfreechart);
 }

 // 生成图表主对象JFreeChart
 public static JFreeChart createChart(DefaultCategoryDataset linedataset) {
  //定义图表对象
  JFreeChart chart = ChartFactory.createLineChart("Vital Sign", // chart title
    "Times", // domain axis label
    "Value", // range axis label
    linedataset, // data
    PlotOrientation.VERTICAL, // orientation
    true, // include legend
    true, // tooltips
    false // urls
    );
  
  
  CategoryPlot plot = chart.getCategoryPlot();
  // customise the range axis...
  NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
  rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
  rangeAxis.setAutoRangeIncludesZero(true);
  rangeAxis.setUpperMargin(0.150);
  rangeAxis.setLabelAngle(Math.PI / 2.0);

  return chart;
 }

 //生成数据
 public DefaultCategoryDataset createDataset() {
  DefaultCategoryDataset linedataset = new DefaultCategoryDataset();
  
  //  各曲线名称
  String series1 = "RR";
  String series2 = "HR";
  String series3 = "BP";
  String series4 = "W";

 
  //    横轴名称(列名称)
  String type1;
  String type2;
  String type3;
  String type4;

  for (VitalSign vs: p.getVitalSignHistory().getVitalSignList()){
      int id;
      id = vs.getId();
      System.out.print(vs.getId());
      type1 = String.valueOf(id);
      linedataset.addValue((vs.getResrate()),series1,type1);
  }

  
  
  for (VitalSign vs: p.getVitalSignHistory().getVitalSignList()){
      int id;
      id = vs.getId();
      type2 = String.valueOf(id);
      linedataset.addValue((vs.getHeartrate()),series2,type2);
  }

    for (VitalSign vs: p.getVitalSignHistory().getVitalSignList()){
      int id;
      id = vs.getId();
      type3 = String.valueOf(id);
      linedataset.addValue((vs.getBloodrate()),series3,type3);
  }
    for (VitalSign vs: p.getVitalSignHistory().getVitalSignList()){
      int id;
      id = vs.getId();
      type4 = String.valueOf(id);
      linedataset.addValue((vs.getWeight()),series4,type4);
  }

  
  return linedataset;
 }
 
 
}