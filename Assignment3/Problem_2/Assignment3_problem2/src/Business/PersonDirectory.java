/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class PersonDirectory {
    
    private ArrayList<Person> personList;
    
    public PersonDirectory(){
        this.personList = new ArrayList<>();
    }
    
    public Person addPerson(){
       
        Person person = new Person();
        personList.add(person);
        return person;
    }
        public void deletePerson(Person person){
        personList.remove(person);
    }
    
    public ArrayList<Person> getPersonList(){
        return personList;
    }
    
    
        public Person searchPerson(String keyWord){
        for (Person person: personList){
        if (person.getPatientname().equals(keyWord)){
            return person;
        }
    }
        return null;
    }
    @Override
    public String toString(){
        return "Person";
    }
    
}
