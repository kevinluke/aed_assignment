/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class SupplierDirectory {
    
     private ArrayList<Supplier> supplierDirectory;
     private DrugCatalog drugCatalog;
    
    public SupplierDirectory(){
        this.supplierDirectory = new ArrayList<>();
        
        Supplier s1 = new Supplier();
        Supplier s2 = new Supplier();
        Supplier s3 = new Supplier();
        s1.setSuppliername("Supplier1");
        s2.setSuppliername("Supplier2");
        s3.setSuppliername("Supplier3");

        DrugCatalog dc1 = new DrugCatalog();
        dc1.getDrugCatalog().get(0).setPharma("Supplier1");
        dc1.getDrugCatalog().get(0).setPrice((float)9.10);
        dc1.getDrugCatalog().get(1).setPharma("Supplier1");
        dc1.getDrugCatalog().get(1).setPrice((float)10.10);
        dc1.getDrugCatalog().get(2).setPharma("Supplier1");
        dc1.getDrugCatalog().get(2).setPrice((float)11.10);
        dc1.getDrugCatalog().get(3).setPharma("Supplier1");
        dc1.getDrugCatalog().get(3).setPrice((float)12.10);
        dc1.getDrugCatalog().get(4).setPharma("Supplier1");
        dc1.getDrugCatalog().get(4).setPrice((float)13.10);
        
        DrugCatalog dc2 = new DrugCatalog();
        dc2.getDrugCatalog().get(1).setPrice((float)220.20);
        dc2.getDrugCatalog().get(1).setPharma("Supplier2");
        dc2.getDrugCatalog().get(2).setPrice((float)230.20);
        dc2.getDrugCatalog().get(2).setPharma("Supplier2");
        dc2.getDrugCatalog().get(3).setPrice((float)120.20);
        dc2.getDrugCatalog().get(3).setPharma("Supplier2");
        dc2.getDrugCatalog().get(4).setPrice((float)320.20);
        dc2.getDrugCatalog().get(4).setPharma("Supplier2");
        dc2.getDrugCatalog().get(0).setPrice((float)520.20);
        dc2.getDrugCatalog().get(0).setPharma("Supplier2");
        
        DrugCatalog dc3 = new DrugCatalog();
        dc3.getDrugCatalog().get(0).setPrice((float)3230.30);
        dc3.getDrugCatalog().get(0).setPharma("Supplier3");
        dc3.getDrugCatalog().get(1).setPrice((float)3120.30);
        dc3.getDrugCatalog().get(1).setPharma("Supplier3");
        dc3.getDrugCatalog().get(2).setPrice((float)3320.30);
        dc3.getDrugCatalog().get(2).setPharma("Supplier3");
        dc3.getDrugCatalog().get(3).setPrice((float)3550.30);
        dc3.getDrugCatalog().get(3).setPharma("Supplier3");
        dc3.getDrugCatalog().get(4).setPrice((float)3520.30);
        dc3.getDrugCatalog().get(4).setPharma("Supplier3");
        
        s1.setDrugCatalog(dc1);
        s2.setDrugCatalog(dc2);
        s3.setDrugCatalog(dc3);
        
        supplierDirectory.add(s1);
        supplierDirectory.add(s2);
        supplierDirectory.add(s3);
        
        
    }
    
    public Supplier addSupplier(){
       
        Supplier supplier = new Supplier();
        supplierDirectory.add(supplier);
        return supplier;
    }
        public void deleteSupplier(Supplier supplier){
        supplierDirectory.remove(supplier);
    }
    
    public ArrayList<Supplier> getsupplierDirectory(){
        return supplierDirectory;
    }
    
    
        public Supplier searchSupplier(String keyWord){
        for (Supplier supplier: supplierDirectory){
        if (supplier.getSuppliername().equals(keyWord)){
            return supplier;
        }
    }
        return null;
    }
    @Override
    public String toString(){
        return "Supplier";
    }
    
}
