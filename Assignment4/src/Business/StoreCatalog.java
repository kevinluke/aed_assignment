/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import Business.Drug;
import Business.DrugInventory;

/**
 *
 * @author 健行
 */
public class StoreCatalog {
     private ArrayList<Store> StoreList;
     private SupplierDirectory supplierDirectory;
    
    public StoreCatalog(){
        this.StoreList = new ArrayList<>();
        this.supplierDirectory = new SupplierDirectory();
//11111111111111111111111
        Store store1 = new Store();
        DrugInventory di1 = new DrugInventory();

        DrugItem DI1 = new DrugItem();
        DrugItem DI2 = new DrugItem();
        DrugItem DI3 = new DrugItem();
        DI1.setDrug(supplierDirectory.getsupplierDirectory().get(0).getDrugCatalog().getDrugCatalog().get(1));
        DI1.setQuantity(192);
        DI2.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(4));
        DI2.setQuantity(12);
        DI3.setDrug(supplierDirectory.getsupplierDirectory().get(1).getDrugCatalog().getDrugCatalog().get(0));
        DI3.setQuantity(2);
        
        di1.addDrugto(DI1);
        di1.addDrugto(DI2);
        di1.addDrugto(DI3);

        store1.setDrugInventory(di1);
        store1.setLocation("Chinatown");
        store1.setName("CVS1");
        StoreList.add(store1);
//22222222222222222222222222222        
        Store store2 = new Store();
        DrugInventory di2 = new DrugInventory();
        
        DrugItem DI4 = new DrugItem();
        DrugItem DI5 = new DrugItem();
        DrugItem DI6 = new DrugItem();
        DI4.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(3));
        DI4.setQuantity(911);
        DI5.setDrug(supplierDirectory.getsupplierDirectory().get(1).getDrugCatalog().getDrugCatalog().get(2));
        DI5.setQuantity(617);
        DI6.setDrug(supplierDirectory.getsupplierDirectory().get(1).getDrugCatalog().getDrugCatalog().get(1));
        DI6.setQuantity(93);
        
        di2.addDrugto(DI4);
        di2.addDrugto(DI5);
        di2.addDrugto(DI6);

        store2.setDrugInventory(di2);
        store2.setLocation("Englewood Ave.");
        store2.setName("CVS2");
        StoreList.add(store2);
 //333333333333333333333333333333333333333333       
        Store store3 = new Store();
        DrugInventory di3 = new DrugInventory();

        DrugItem DI7 = new DrugItem();
        DrugItem DI8 = new DrugItem();
        DrugItem DI9 = new DrugItem();
        DI7.setDrug(supplierDirectory.getsupplierDirectory().get(0).getDrugCatalog().getDrugCatalog().get(2));
        DI7.setQuantity(6);
        DI8.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(3));
        DI8.setQuantity(2);
        DI9.setDrug(supplierDirectory.getsupplierDirectory().get(0).getDrugCatalog().getDrugCatalog().get(4));
        DI9.setQuantity(3);
        
        di3.addDrugto(DI7);
        di3.addDrugto(DI8);
        di3.addDrugto(DI9);

        
        store3.setDrugInventory(di3);
        store3.setLocation("Northestern University");
        store3.setName("CVS3");
        StoreList.add(store3);
    }
    
    public Store addStore(){
       
        Store store = new Store();
        StoreList.add(store);
        return store;
    }
        public void deleteStore(Store store){
        StoreList.remove(store);
    }
    
    public ArrayList<Store> getStoreList(){
        return StoreList;
    }
    
    
        public Store searchStore(String keyWord){
        for (Store store: StoreList){
        if (store.getName().equals(keyWord)){
            return store;
        }
    }
        return null;
    }
    @Override
    public String toString(){
        return "Store";
    }
    
}
