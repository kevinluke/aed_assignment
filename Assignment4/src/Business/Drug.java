/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author 健行
 */
public class Drug {
    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    private String name;
    private String pharma;
    private float price;
    private int serialNum;
    private String date;
    private String workfor;
    private boolean RX;
    private String model;
    private String howtoeat;
    private int minage;
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPharma() {
        return pharma;
    }

    public void setPharma(String pharma) {
        this.pharma = pharma;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(int serialNum) {
        this.serialNum = serialNum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWorkfor() {
        return workfor;
    }

    public void setWorkfor(String workfor) {
        this.workfor = workfor;
    }

    public boolean isRX() {
        return RX;
    }

    public void setRX(boolean RX) {
        this.RX = RX;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getHowtoeat() {
        return howtoeat;
    }

    public void setHowtoeat(String howtoeat) {
        this.howtoeat = howtoeat;
    }

    public int getMinage() {
        return minage;
    }

    public void setMinage(int minage) {
        this.minage = minage;
    }
    
    @Override
    public String toString(){
        return this.name;
    }
    
}


