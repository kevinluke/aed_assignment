/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class DrugCatalog {
     private ArrayList<Drug> drugCatalog;
     private SupplierDirectory supplierDirectory;
    
    public DrugCatalog(){
        this.drugCatalog = new ArrayList<>();
        

        Drug drug1 = new Drug();
        Drug drug2 = new Drug();
        Drug drug3 = new Drug();
        Drug drug4 = new Drug();
        Drug drug5 = new Drug();
        
        drug1.setDate("2018/9/11");
        drug1.setHowtoeat("Drink");
        drug1.setMinage(5);
        drug1.setModel("XXS!23");
        drug1.setName("DSB");
                drug1.setFloor((float)100);
        drug1.setCeiling((float)200);
        drug1.setTarget((float)150);
        drug1.setRX(true);
        drug1.setSerialNum(12345);
        drug1.setWorkfor("Cold");
        drugCatalog.add(drug1);
        
        drug2.setDate("2017/6/17");
        drug2.setHowtoeat("Eat");
        drug2.setMinage(18);
        drug2.setModel("XXS231");
        drug2.setName("LGD");
        drug2.setFloor((float)50);
        drug2.setCeiling((float)100);
        drug2.setTarget((float)80);
        drug2.setRX(false);
        drug2.setSerialNum(23456);
        drug2.setWorkfor("High Pressure");
        drugCatalog.add(drug2);
        
        
        drug3.setDate("2015/12/4");
        drug3.setHowtoeat("Drink");
        drug3.setMinage(1);
        drug3.setModel("LAZ1314");
        drug3.setName("EDG");
        drug3.setFloor((float)300);
        drug3.setCeiling((float)400);
        drug3.setTarget((float)350);
        drug3.setRX(true);
        drug3.setSerialNum(34567);
        drug3.setWorkfor("Cold");
        drugCatalog.add(drug3);
        //-------------------------------------
        
        
        drug4.setDate("2015/12/4");
        drug4.setHowtoeat("Drink");
        drug4.setMinage(1);
        drug4.setModel("LAZ1314");
        drug4.setName("EDG11");
        drug4.setFloor((float)400);
        drug4.setCeiling((float)500);
        drug4.setTarget((float)450);
        drug4.setRX(true);
        drug4.setSerialNum(34567);
        drug4.setWorkfor("Cold");
        drugCatalog.add(drug4);
        
        drug5.setDate("2015/12/4");
        drug5.setHowtoeat("Drink");
        drug5.setMinage(1);
        drug5.setModel("LAZ1314");
        drug5.setName("EDG11111");
        drug5.setRX(true);
        drug5.setFloor((float)200);
        drug5.setCeiling((float)300);
        drug5.setTarget((float)250);
        drug5.setSerialNum(34567);
        drug5.setWorkfor("Cold");
        drugCatalog.add(drug5);
        
    }
    
    public Drug addDrug(){
       
        Drug drug = new Drug();
        drugCatalog.add(drug);
        return drug;
    }
        public void deleteDrug(Drug drug){
        drugCatalog.remove(drug);
    }
    
    public ArrayList<Drug> getDrugCatalog(){
        return drugCatalog;
    }
    
    
        public Drug searchDrug(String keyWord){
        for (Drug drug: drugCatalog){
        if (drug.getName().equals(keyWord)){
            return drug;
        }
    }
        return null;
    }
    @Override
    public String toString(){
        return "Drug";
    }
    
}
