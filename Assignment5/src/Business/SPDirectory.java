/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class SPDirectory {
    
      private ArrayList<SalesPerson> spDirectory;

      private SupplierDirectory supplierDirectory;

    public SPDirectory(){
        
        this.spDirectory = new ArrayList<>();
        
        this.supplierDirectory = new SupplierDirectory();
        SalesPerson store1 = new SalesPerson();
        DrugInventory di1 = new DrugInventory();

        DrugItem DI1 = new DrugItem();
        DrugItem DI2 = new DrugItem();
        DrugItem DI3 = new DrugItem();
        DI1.setDrug(supplierDirectory.getsupplierDirectory().get(0).getDrugCatalog().getDrugCatalog().get(0));
        DI1.setQuantity(192);
        DI1.getDrug().setNprice((float)152);
        DI2.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(2));
        DI2.setQuantity(12);
        DI2.getDrug().setNprice((float)348);
        DI3.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(1));
        DI3.setQuantity(2);
        DI3.getDrug().setNprice((float)75);
        
        di1.addDrugto(DI1);
        di1.addDrugto(DI2);
        di1.addDrugto(DI3);

        store1.setDrugInventory(di1);
       
        store1.setName("sp1");
        spDirectory.add(store1);
        

        SalesPerson store2 = new SalesPerson();
        DrugInventory di2 = new DrugInventory();

        DrugItem DI4 = new DrugItem();
        DrugItem DI5 = new DrugItem();
        DrugItem DI6 = new DrugItem();
        DI4.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(3));
        DI4.setQuantity(12);
        DI4.getDrug().setNprice((float)480);
        DI5.setDrug(supplierDirectory.getsupplierDirectory().get(1).getDrugCatalog().getDrugCatalog().get(4));
        DI5.setQuantity(232);
        DI5.getDrug().setNprice((float)290);
        DI6.setDrug(supplierDirectory.getsupplierDirectory().get(1).getDrugCatalog().getDrugCatalog().get(2));
        DI6.setQuantity(44);
        DI6.getDrug().setNprice((float)356);
        
        di2.addDrugto(DI4);
        di2.addDrugto(DI5);
        di2.addDrugto(DI6);

        store2.setDrugInventory(di2);
        
        store2.setName("sp2");
        spDirectory.add(store2);
        

        SalesPerson store3 = new SalesPerson();
        DrugInventory di3 = new DrugInventory();

        DrugItem DI7 = new DrugItem();
        DrugItem DI8 = new DrugItem();
        DrugItem DI9 = new DrugItem();
        DI7.setDrug(supplierDirectory.getsupplierDirectory().get(0).getDrugCatalog().getDrugCatalog().get(2));
        DI7.setQuantity(99);
        DI7.getDrug().setNprice((float)340);
        DI8.setDrug(supplierDirectory.getsupplierDirectory().get(1).getDrugCatalog().getDrugCatalog().get(1));
        DI8.setQuantity(100);
        DI8.getDrug().setNprice((float)75);
        DI9.setDrug(supplierDirectory.getsupplierDirectory().get(1).getDrugCatalog().getDrugCatalog().get(4));
        DI9.setQuantity(101);
        DI9.getDrug().setNprice((float)210);
        
        di3.addDrugto(DI7);
        di3.addDrugto(DI8);
        di3.addDrugto(DI9);

        store3.setDrugInventory(di3);
        
        
        store3.setName("sp3");
        spDirectory.add(store3);
        
        SalesPerson store4 = new SalesPerson();
        DrugInventory di4 = new DrugInventory();

        DrugItem DI10 = new DrugItem();
        DrugItem DI11 = new DrugItem();
        DrugItem DI12 = new DrugItem();
        DI10.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(2));
        DI10.setQuantity(55);
        DI10.getDrug().setNprice((float)349);
        DI11.setDrug(supplierDirectory.getsupplierDirectory().get(2).getDrugCatalog().getDrugCatalog().get(1));
        DI11.setQuantity(66);
        DI11.getDrug().setNprice((float)25);
        DI12.setDrug(supplierDirectory.getsupplierDirectory().get(0).getDrugCatalog().getDrugCatalog().get(4));
        DI12.setQuantity(1177);
        DI12.getDrug().setNprice((float)256);
        
        di4.addDrugto(DI10);
        di4.addDrugto(DI11);
        di4.addDrugto(DI12);

        store4.setDrugInventory(di4);
        
        store4.setName("sp4");
        spDirectory.add(store4);
        
        
        
        
        
    }

        
    public SalesPerson addSP(SalesPerson salesPerson){
       
        
        spDirectory.add(salesPerson);
        return salesPerson;
    }
        public void deleteSP(SalesPerson salesPerson){
        spDirectory.remove(salesPerson);
    }
    
    public ArrayList<SalesPerson> getSPDirectory(){
        return spDirectory;
    }
    
    
        public SalesPerson searchSP(String keyWord){
        for (SalesPerson salesPerson: spDirectory){
        if (salesPerson.getName().equals(keyWord)){
            return salesPerson;
        }
    }
        return null;
    }
    @Override
    public String toString(){
        return "Item";
    }
    
}
