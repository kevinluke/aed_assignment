/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.User;

import Business.Card.Card;
import Business.Enterprise.TransportEnterprise;
import Business.Location.Location;
import Business.Organization.Organization;
import Business.Organization.TaxiOrganization;
import Business.Transport.Transport;
import Business.Transport.TransportDirectory;
import Business.Transport.Vehicle;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.TaxiWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ConcurrentModificationException;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author 健行
 */
public class UserWorkAreaJPanel extends javax.swing.JPanel {

    JPanel userProcessContainer;
    TransportEnterprise business;
    UserAccount account;
    /**
     * Creates new form UserWorkAreaJPanel
     */
    public UserWorkAreaJPanel(JPanel userProcessContainer, TransportEnterprise business, UserAccount account) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.account = account;
        populateFrom();
        populateTo();
        byBox.removeAllItems();
        populatePanel();
        populateTaxi();
    }
    
    public void populateTaxi(){
        if (account.getWorkQueue().getWorkRequestList().size()==1||account.getWorkQueue().getWorkRequestList().size()==2){
            for (WorkRequest request1 : account.getWorkQueue().getWorkRequestList()){
                if (request1.getType().equals("Taxi")){
                    if(request1.getStatus().equals("Y")){
                        statusTxt.setText("Accepted");
                        priceTxt.setText(request1.getMessage());
                    }                                 
                    else if(request1.getStatus().equals("Sent")){
                        statusTxt.setText("Sent");
                        priceTxt.setText(request1.getMessage());
                    }
                    else if(request1.getStatus().equals("Pending")){
                        statusTxt.setText("Pending");
                        priceTxt.setText(request1.getMessage());
                    }
                    else{
                        statusTxt.setText("Denied");
                        priceTxt.setEditable(true);
                        
                    } 
                }
                
            }
            priceBox.removeAllItems();
            for (Card c : account.getUser().getCardDirectory().getCardList()){
                if (c.getCardType().equals("Money")){
                    priceBox.addItem(c.getCardid() + "-" +c);
                }
            }
            
        }
        else {
            priceBox.removeAllItems();
            statusTxt.setText("");
            priceTxt.setText("");
        }
    }

    public void populatePanel(){
        
        oneTxt.setEditable(false);
        twoTxt.setEditable(false);
        poneTxt.setEditable(false);
        ptwoTxt.setEditable(false);
        pthreeTxt.setEditable(false);
        priceTxt.setEditable(false);
        statusTxt.setEditable(false);
        
    }
    public void populateFrom(){
        fromBox.removeAllItems();
        
        TransportDirectory td = new TransportDirectory();
        for (Transport t: td.getTransportList()){
            for (Vehicle v: t.getVehicleCatalog().getVehicleList()){
                for(Location l : v.getLc().getLocationList()){
                    fromBox.addItem(l + "-" + v.getVname());
                    
                }
            }
        }
        fromBox.addItem("Other Place");
    }
    
    public void populateBy(String s){
        
        byBox.removeAllItems();
        TransportDirectory td = new TransportDirectory();
        
        if (s.equals("Other Place")){
            byBox.addItem(td.getTransportList().get(2));
        }    
        else {
            
            for (Transport t: td.getTransportList()){
                byBox.addItem(t);
            }
            
        }
        
    }
    
    public void populateTo(){
        toBox.removeAllItems();
        
        TransportDirectory td = new TransportDirectory();
        for (Transport t: td.getTransportList()){
            for (Vehicle v: t.getVehicleCatalog().getVehicleList()){
                for(Location l : v.getLc().getLocationList()){
                    toBox.addItem(l + "-" + v.getVname());
                    
                }
            }
        }
        toBox.addItem("Other Place");
    }
    
  /*  public void isChange(Location l2){
        if (l2.getIsChange().equals("Y")){
            
        }
        else {
            FindVehicleFromN(l2);
        }
    }
    */
    
    
    
    public Location FindLocation(String l, String v){
        
        TransportDirectory td = new TransportDirectory();
        
        for (Transport t : td.getTransportList()){
            for(Vehicle ve : t.getVehicleCatalog().getVehicleList()){
                if(ve.getVname().equals(v)){
                    for(Location lo : ve.getLc().getLocationList()){
                        if (lo.getLocationname().equals(l)){
                            return lo;
                        }
                    }
                }
            }
        }
        return null;
    }
    public Card FindCard(int id){
        
        for (Card c :account.getUser().getCardDirectory().getCardList()){
            if (c.getCardid() == id){
                return c;
            }
        }
        return null;
    }
    
    public Vehicle FindVehicle(String l, String v){
        
        TransportDirectory td = new TransportDirectory();
        
        for (Transport t : td.getTransportList()){
            for(Vehicle ve : t.getVehicleCatalog().getVehicleList()){
                if(ve.getVname().equals(v)){
                    return ve;
                }
            }
        }
        return null;
    }

    
    /*public Vehicle FindVehicleFromN(Location l1){
        
        TransportDirectory td = new TransportDirectory();
        if(l1.getIsChange().equals("Y")){
            
        }
        else{
            Vehicle vehicle = new Vehicle();
            for (Transport t: td.getTransportList()){
                for (Vehicle v: t.getVehicleCatalog().getVehicleList()){
                    for(Location l : v.getLc().getLocationList()){
                        if (l.equals(l1)){ 
                            return v;
                        }
                    }
                }
            }
        }
        return null;
    }
    */

    /*public Vehicle ForBreak(Location l1){
        
        TransportDirectory td = new TransportDirectory();
        
        for (Transport t: td.getTransportList()){
            for (Vehicle v: t.getVehicleCatalog().getVehicleList()){
                for(Location l : v.getLc().getLocationList()){
                    if ( l.equals(l1)){ 
                        
                        return v;
                    }
                }
            }
        }
        return null;
    }
    */
    
    public int Count(Location l1, Vehicle v1, Location l2, Vehicle v2){
        
        TransportDirectory td = new TransportDirectory();
        
        int count = 0;
        
        if (v1.getVname().equals(v2.getVname())){
            return count+1;
        }
        
        else {
            for (Location lo1: v1.getLc().getLocationList()){
                for (Location lo2: v2.getLc().getLocationList()){
                    if (lo1.getLocationname().equals(lo2.getLocationname())){
                        return count+2;
                    }
                }
            }
        }
        
        return 0;
    }
    
    public Location FindTVehicleToPTwo(Location l1, Vehicle v1, Location l2, Vehicle v2){
        TransportDirectory td = new TransportDirectory();
        for (Location lo1 : v1.getLc().getLocationList()){
                if (lo1.getIsChange().equals("Y")){
                    td.getTransportList().get(0).getVehicleCatalog().deleteVehicle(v1);
                    for(Vehicle v : td.getTransportList().get(0).getVehicleCatalog().getVehicleList()){
                        for(Location l : v.getLc().getLocationList()){
                            if (l.getLocationname().equals(lo1.getLocationname())){
                                
                                for (Location loc1 : v.getLc().getLocationList()){
                                    for (Location loc2 : v2.getLc().getLocationList()){
                                        if (loc1.getLocationname().equals(loc2.getLocationname())){
                                            return loc1;
                                        }
                                        
                                    }
                                }
                                
                                
                            }
                        }
                    }
                }
            }
        return null;
    }
    
    public Vehicle FindTVehicleToThree(Location l1, Vehicle v1, Location l2, Vehicle v2){
        TransportDirectory td = new TransportDirectory();
        for (Location lo1 : v1.getLc().getLocationList()){
                if (lo1.getIsChange().equals("Y")){
                    td.getTransportList().get(0).getVehicleCatalog().deleteVehicle(v1);
                    for(Vehicle v : td.getTransportList().get(0).getVehicleCatalog().getVehicleList()){
                        for(Location l : v.getLc().getLocationList()){
                            if (l.getLocationname().equals(lo1.getLocationname())){
                                
                                for (Location loc1 : v.getLc().getLocationList()){
                                    for (Location loc2 : v2.getLc().getLocationList()){
                                        if (loc1.getLocationname().equals(loc2.getLocationname())){
                                            return v;
                                        }
                                        
                                    }
                                }
                                
                                
                            }
                        }
                    }
                }
            }
        
        return null;
    }
    
    public Location FindTVehicleToPOne(Location l1, Vehicle v1, Location l2, Vehicle v2){
        
        TransportDirectory td = new TransportDirectory();
        
        
            for (Location lo1: v1.getLc().getLocationList()){
                for (Location lo2: v2.getLc().getLocationList()){
                    if (lo1.getLocationname().equals(lo2.getLocationname())){
                        return lo1;
                    }
                }
            }
            
            for (Location lo1 : v1.getLc().getLocationList()){
                if (lo1.getIsChange().equals("Y")){
                    td.getTransportList().get(0).getVehicleCatalog().deleteVehicle(v1);
                    for(Vehicle v : td.getTransportList().get(0).getVehicleCatalog().getVehicleList()){
                        for(Location l : v.getLc().getLocationList()){
                            if (l.getLocationname().equals(lo1.getLocationname())){
                                
                                for (Location loc1 : v.getLc().getLocationList()){
                                    for (Location loc2 : v2.getLc().getLocationList()){
                                        if (loc1.getLocationname().equals(loc2.getLocationname())){
                                            return lo1;
                                        }
                                        
                                    }
                                }
                                
                                
                            }
                        }
                    }
                }
            }
            
            
       /* 
        System.out.print(vehicle.getVname());
        
        
        
        //step1
        
            for (Location l : vehicle.getLc().getLocationList()){
                if (l.equals(l2)){
                    return vehicle;
                }
                
                
                
                
                
                
            else{
                for(Location la: vehicle.getLc().getLocationList()){
                    if(la.getIsChange().equals("Y")){
                        td.getTransportList().get(0).getVehicleCatalog().deleteVehicle(vehicle);
                        Vehicle vehicle1 = new Vehicle();
                        for(Vehicle v : td.getTransportList().get(0).getVehicleCatalog().getVehicleList()){
                            for(Location lb: v.getLc().getLocationList()){
                                if(lb.getLocationname().equals(la.getLocationname())){
                                    for(Location lc: v.getLc().getLocationList()){
                                        if(lc.equals(lb)){
                                            
                                        }
                                    }
                                }
                                
                            }
                        }

                    }
                    
                }
            }
        }
        */
        
        return null;
    }
    
    public void Refresh(){
        
            oneTxt.setText("");
            twoTxt.setText("");
            poneTxt.setText("");
            ptwoTxt.setText("");
            pthreeTxt.setText("");
            priceTxt.setText("");
            statusTxt.setText("");
            
   
    }
    
    public void populatePrice(){
        priceBox.removeAllItems();
        if (!priceTxt.isEditable()){
            for (Card c : account.getUser().getCardDirectory().getCardList()){
                priceBox.addItem(c.getCardid() + "-" +c);
            }
        }
        else {
            for (Card c : account.getUser().getCardDirectory().getCardList()){
                if (c.getCardType().equals("Money")){
                    priceBox.addItem(c.getCardid() + "-" +c);
                }
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        fromBox = new javax.swing.JComboBox();
        toBox = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        byBox = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        oneTxt = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        twoTxt = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        poneTxt = new javax.swing.JTextField();
        ptwoTxt = new javax.swing.JTextField();
        pthreeTxt = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        priceTxt = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        priceBox = new javax.swing.JComboBox();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        statusTxt = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Showcard Gothic", 1, 36)); // NOI18N
        jLabel1.setText("Welcome!!!");

        jLabel2.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel2.setText("From:");

        jLabel3.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel3.setText("To:");

        fromBox.setFont(new java.awt.Font("Eras Bold ITC", 0, 12)); // NOI18N
        fromBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        fromBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fromBoxActionPerformed(evt);
            }
        });

        toBox.setFont(new java.awt.Font("Eras Bold ITC", 0, 12)); // NOI18N
        toBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        toBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toBoxActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel4.setText("By:");

        byBox.setFont(new java.awt.Font("Eras Bold ITC", 0, 12)); // NOI18N
        byBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        byBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                byBoxActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton1.setText("Search");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel5.setText("Path:");

        oneTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jLabel6.setText(">>>>>");

        twoTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jLabel7.setText(">>>>>");

        poneTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        ptwoTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        pthreeTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel8.setText("Price:");

        priceTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel9.setText("$");

        jLabel10.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel10.setText("Payment:");

        jLabel11.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel11.setText("By");

        priceBox.setFont(new java.awt.Font("Eras Bold ITC", 0, 12)); // NOI18N
        priceBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        priceBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                priceBoxActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton2.setText("Confirm");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton3.setText("Manage My Cards");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        statusTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel13.setText("Status(for Taxi):");

        jButton4.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton4.setText("Submit");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel14.setText("By");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(oneTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63)
                        .addComponent(twoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(fromBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(22, 22, 22)
                                    .addComponent(toBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(110, 110, 110)
                            .addComponent(jLabel4)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(byBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(poneTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel6)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(ptwoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(10, 10, 10)
                                            .addComponent(jLabel11)))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel7)
                                            .addGap(18, 18, 18)
                                            .addComponent(pthreeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(10, 10, 10)
                                            .addComponent(jLabel14))))
                                .addComponent(jLabel1))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(priceTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(jLabel13))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addComponent(priceBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(35, 35, 35)
                                .addComponent(jButton2)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton3)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(statusTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton4)))))
                .addContainerGap(131, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(fromBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(toBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(byBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1))
                        .addGap(42, 42, 42)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(poneTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(ptwoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(pthreeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(twoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(oneTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(78, 78, 78)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(priceTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(statusTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jButton4))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(priceBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addGap(79, 79, 79))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void fromBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fromBoxActionPerformed

        
        String fromL = (String) fromBox.getSelectedItem();
        
        if (fromL !=null){
            populateBy(fromL); 

        }
        
    }//GEN-LAST:event_fromBoxActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    try{    
        populatePrice();
        Refresh();
        Transport transport = (Transport) byBox.getSelectedItem();
        String transportname = transport.getName();
        
        if (transportname.equals("T")){
        
            priceTxt.setEditable(false);
            String fromL = (String) fromBox.getSelectedItem();
            String[] aryf = fromL.split("-");
            String locationf = aryf[0];
            String vehiclef = aryf[1];

            Location lf = FindLocation(locationf, vehiclef);
            Vehicle vf = FindVehicle(locationf, vehiclef);
            
            String toL = (String) toBox.getSelectedItem();
            String[] aryt = toL.split("-");
            String locationt = aryt[0];
            String vehiclet = aryt[1];
        
            Location lt = FindLocation(locationt, vehiclet);
            Vehicle vt = FindVehicle(locationt, vehiclet);
            //---------------------------
            
            int count = Count(lf, vf, lt, vt);
            
            if (count == 1){
                
                poneTxt.setText(vf.getVname());
                priceTxt.setText(String.valueOf(vf.getPrice()));
            }
            
            else if (count == 2){
                
                poneTxt.setText(vf.getVname());
                ptwoTxt.setText(vt.getVname());
                Location lp = FindTVehicleToPOne(lf, vf, lt, vt);
                oneTxt.setText(lp.getLocationname());
                priceTxt.setText(String.valueOf(vf.getPrice()+vt.getPrice()));
            }
            else {
                poneTxt.setText(vf.getVname());
                Vehicle v3 = FindTVehicleToThree(lf, vf, lt, vt);
                ptwoTxt.setText(v3.getVname());
                pthreeTxt.setText(vt.getVname());
                Location lp = FindTVehicleToPOne(lf, vf, lt, vt);
                oneTxt.setText(lp.getLocationname());
                Location lp2 = FindTVehicleToPTwo(lf, vf, lt, vt);
                twoTxt.setText(lp2.getLocationname());
                priceTxt.setText(String.valueOf(vf.getPrice()+vt.getPrice()+v3.getPrice()));
            }
            /*
            Location lp = FindTVehicleTo(lf, vf, lt, vt);
            
            poneTxt.setText(vf.getVname());
            ptwoTxt.setText(vt.getVname());
        
            
            oneTxt.setText(lp.getLocationname());
            */
            
        }
        else if (transportname.equals("Bus")){
            
            priceTxt.setEditable(false);
            String fromL = (String) fromBox.getSelectedItem();
            String[] aryf = fromL.split("-");
            String locationf = aryf[0];
            String vehiclef = aryf[1];

            Location lf = FindLocation(locationf, vehiclef);
            Vehicle vf = FindVehicle(locationf, vehiclef);
            
            String toL = (String) toBox.getSelectedItem();
            String[] aryt = toL.split("-");
            String locationt = aryt[0];
            String vehiclet = aryt[1];
        
            Location lt = FindLocation(locationt, vehiclet);
            Vehicle vt = FindVehicle(locationt, vehiclet);
            //---------------------------
            
            int count = Count(lf, vf, lt, vt);
            
            if (count == 1){
                
                poneTxt.setText(vf.getVname());
                priceTxt.setText(String.valueOf(vf.getPrice()));
            }
            
            else if (count == 2){
                
                poneTxt.setText(vf.getVname());
                ptwoTxt.setText(vt.getVname());
                Location lp = FindTVehicleToPOne(lf, vf, lt, vt);
                oneTxt.setText(lp.getLocationname());
                priceTxt.setText(String.valueOf(vf.getPrice()+vt.getPrice()));
            }
            else {
                poneTxt.setText(vf.getVname());
                Vehicle v3 = FindTVehicleToThree(lf, vf, lt, vt);
                ptwoTxt.setText(v3.getVname());
                pthreeTxt.setText(vt.getVname());
                Location lp = FindTVehicleToPOne(lf, vf, lt, vt);
                oneTxt.setText(lp.getLocationname());
                Location lp2 = FindTVehicleToPTwo(lf, vf, lt, vt);
                twoTxt.setText(lp2.getLocationname());
                priceTxt.setText(String.valueOf(vf.getPrice()+vt.getPrice()+v3.getPrice()));
            }
            
        }
        else {
            
            priceTxt.setEditable(true);
            JOptionPane.showMessageDialog(null, "You have chosen Taxi service, please enter the price after your trip, then click submit button and wait for driver's confirmation!");
            populatePrice();
        }
    }
    catch(NullPointerException e){
        Refresh();
        JOptionPane.showMessageDialog(null, "There is no way to go there, please choose Taxi" );
    }
        

    }//GEN-LAST:event_jButton1ActionPerformed

    
    private void toBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toBoxActionPerformed

        if(byBox.getItemCount()> 1 ){
            
            String toL = (String) toBox.getSelectedItem();
            if (toL !=null){
                populateBy(toL); 
            }    
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_toBoxActionPerformed

    private void byBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_byBoxActionPerformed

     
// TODO add your handling code here:
    }//GEN-LAST:event_byBoxActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

    
        
        String s = (String) priceBox.getSelectedItem();
        if (s != null){
            String[] aryf = s.split("-");
            int card = Integer.parseInt(aryf[0]);
            String type = aryf[1];
            Card c = FindCard(card);
            if (statusTxt.getText().equals("")){
                if (type.equals("Expire")){
                    if (!c.getExpireDate().equals("Not available")){
                        Date d = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
                        try{
                            Date date = sdf.parse(c.getExpireDate());
                            if (date.after(d)){
                                JOptionPane.showMessageDialog(null, "Purchase successfully!");
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "Card Expired at" + " " +c.getExpireDate() + ", " + "use another card please!");

                        }
                        }
                        catch (ParseException e){
                            e.printStackTrace();
                        }
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Please go to 'Manage My Card' and active this card first!");
                    }

            }
                else {

                    float balance = c.getBalance() - Float.parseFloat(priceTxt.getText());
                    if (balance < 0){
                        JOptionPane.showMessageDialog(null, "Your have run out of money, please recharge it!");
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Purchase successfully! Your balance is " + balance);
                        c.setBalance(balance);

                    }
                }
            }

            else {

                if (statusTxt.getText().equals("Accepted")){
                    float balance = c.getBalance() - Float.parseFloat(priceTxt.getText());
                    if (balance < 0){
                        JOptionPane.showMessageDialog(null, "Your have run out of money, please recharge it!");
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Purchase successfully! Your balance is " + balance);
                        c.setBalance(balance);
                        try{
                            for (int i = 0; i <account.getWorkQueue().getWorkRequestList().size();i++){
                                WorkRequest wr =  account.getWorkQueue().getWorkRequestList().get(i);
                                if (wr.getType().equals("Taxi")){
                                    account.getWorkQueue().getWorkRequestList().remove(wr);
                                }
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    JOptionPane.showMessageDialog(null, "Wrong price, please enter again!");
                }

            }


            Refresh();

        }
        else{
            Object[] options ={ "Add now!", "Nope!" };  
            int m = JOptionPane.showOptionDialog(null, "You have no card now, please add one first", "Notice",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            if (m == JOptionPane.YES_NO_OPTION){
                RechargeJPanel processWorkRequestJPanel = new RechargeJPanel(userProcessContainer, account, business);
                userProcessContainer.add("RechargeJPanel", processWorkRequestJPanel);
                CardLayout layout = (CardLayout) userProcessContainer.getLayout();
                layout.next(userProcessContainer);    
            }
            else {
                return;
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        
        RechargeJPanel processWorkRequestJPanel = new RechargeJPanel(userProcessContainer, account, business);
        userProcessContainer.add("RechargeJPanel", processWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);    
        
        
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void priceBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_priceBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_priceBoxActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed

        String message = priceTxt.getText();
        if(message.equals("") || message.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please enter price.");
            return;
        }
        TaxiWorkRequest request = new TaxiWorkRequest();
        request.setMessage(message);
        request.setSender(account);
        request.setStatus("Sent");
        request.setType("Taxi");
        
        Organization org = null;
        for (Organization organization : business.getOrganizationDirectory().getOrganizationList()){
            if (organization instanceof TaxiOrganization){
                org = organization;
                break;
            }
        }
        if (org!=null){
            org.getWorkQueue().getWorkRequestList().add(request);
            if (account.getWorkQueue().getWorkRequestList().size()<1){
                account.getWorkQueue().getWorkRequestList().add(request);
            }
            else{
                for (int i =0 ; i < account.getWorkQueue().getWorkRequestList().size() ; i++){
                    WorkRequest wr = account.getWorkQueue().getWorkRequestList().get(i);
                    if (wr.getType().equals("Taxi")){
                        account.getWorkQueue().getWorkRequestList().remove(wr);
                        account.getWorkQueue().getWorkRequestList().add(request);
                    }
                    else {
                        account.getWorkQueue().getWorkRequestList().add(request);
                    }
                }
            }

        }
        
        JOptionPane.showMessageDialog(null, "Request message sent");
        
        for (WorkRequest request1 : account.getWorkQueue().getWorkRequestList()){
            statusTxt.setText(request1.getStatus());
            System.out.print("222");
        }
         System.out.print(account.getWorkQueue().getWorkRequestList().size());
    }//GEN-LAST:event_jButton4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox byBox;
    private javax.swing.JComboBox fromBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField oneTxt;
    private javax.swing.JTextField poneTxt;
    private javax.swing.JComboBox priceBox;
    private javax.swing.JTextField priceTxt;
    private javax.swing.JTextField pthreeTxt;
    private javax.swing.JTextField ptwoTxt;
    private javax.swing.JTextField statusTxt;
    private javax.swing.JComboBox toBox;
    private javax.swing.JTextField twoTxt;
    // End of variables declaration//GEN-END:variables
}
