/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Card;

import Business.User.User;

/**
 *
 * @author 健行
 */

// Constructor of Card, which is stored in CardDirectory class. There are two 
// types of card, one is expired card, another is balance card.
public class Card {
    
    private int cardid;
    private String expireDate;
    private float balance;
    private String cardType;



    public int getCardid() {
        return cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    
    @Override
    public String toString(){
        return cardType;
    }
    
}
