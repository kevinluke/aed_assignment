/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.User;

import Business.Card.Card;
import Business.Card.CardDirectory;

/**
 *
 * @author 健行
 */
public class User {
    
    private String username;
    private int userid;
    private static int count = 1;
    private CardDirectory cardDirectory;
    
    public User(){
        cardDirectory = new CardDirectory();
        userid = count;
        count++;
}

    public CardDirectory getCardDirectory() {
        return cardDirectory;
    }

    public void setCardDirectory(CardDirectory cardDirectory) {
        this.cardDirectory = cardDirectory;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Override
    public String toString(){
        return username;
    }
}
