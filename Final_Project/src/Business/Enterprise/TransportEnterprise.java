/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;


import Business.Enterprise.Enterprise;
import Business.Organization.OrganizationDirectory;
import Business.Role.Role;
import Business.Transport.TransportDirectory;
import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class TransportEnterprise extends Enterprise{
    
    private static TransportEnterprise business;
    private OrganizationDirectory organizationDirectory;
    private TransportDirectory transportDirectory;

    
    
    public static TransportEnterprise getInstance(){
        if (business == null){
            business = new TransportEnterprise("Transport Company");
        }
        return business;
    }

    public TransportEnterprise(String name) {
        super(name, EnterpriseType.TransportCompany);
        organizationDirectory = new OrganizationDirectory();
        transportDirectory = new TransportDirectory();
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }
    
    public TransportDirectory getTransportDirectory() {
        return transportDirectory;
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
    
}
