/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class WorkQueue {
    
    private ArrayList<WorkRequest> workRequestList;

    public WorkQueue() {
        workRequestList = new ArrayList<>();
    }

    public ArrayList<WorkRequest> getWorkRequestList() {
        return workRequestList;
    }
    public WorkRequest searchDrug(String keyWord){
        for (WorkRequest wr: workRequestList){
            if (wr.getSender().equals(keyWord)){
                return wr;
            }
        }
        return null;
    }
}