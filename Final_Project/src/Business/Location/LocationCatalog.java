/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Location;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class LocationCatalog {
    private ArrayList<Location> LocationList;
    
    public LocationCatalog(){
        this.LocationList = new ArrayList<>();
    }
    
    public Location addLocation(Location location){
        
        LocationList.add(location);
        return location;
    }
    
    public void deleteLocation(Location location){
        LocationList.remove(location);
    }

    public ArrayList<Location> getLocationList() {
        return LocationList;
    }
    
    
}
