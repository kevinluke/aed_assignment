package Business;


import Business.Enterprise.Enterprise;
import Business.Enterprise.TransportEnterprise;
import Business.Network.Network;
import Business.Role.AdminRole;
import Business.Role.SystemAdminRole;
import Business.Role.TaxiRole;
import Business.Role.UserRole;
import Business.User.User;
import Business.UserAccount.UserAccount;

/**
 *
 * @author rrheg
 */
public class ConfigureASystem {
    
    public static EcoSystem configure(){
        
        EcoSystem system = EcoSystem.getInstance();

        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        
        
        User user = system.getUserDirectory().createUser("Jianxing Lu");
        
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", user, new SystemAdminRole());
        

        return system;
    }
    
}
