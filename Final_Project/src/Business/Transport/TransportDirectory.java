/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Transport;

import Business.Location.Location;
import Business.Location.LocationCatalog;
import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class TransportDirectory {
    private ArrayList<Transport> transportList;

    public TransportDirectory() {
        transportList = new ArrayList<>();
        Transport t1 = new Transport("T");
        
        Location l1 = new Location();
        Location l2 = new Location();
        Location l3 = new Location();
        Location l4 = new Location();
        Location l5 = new Location();
        Location l6 = new Location();
        Location l7 = new Location();
        Location l8 = new Location();
        Location l9 = new Location();
        Location l10 = new Location();

        l1.setIsChange("Y");
        l1.setLocationname("State");
        l2.setIsChange("N");
        l2.setLocationname("Aquarium");
        l3.setIsChange("N");
        l3.setLocationname("Airport");
        l4.setIsChange("Y");
        l4.setLocationname("North Station");
        l5.setIsChange("N");
        l5.setLocationname("Copley");
        l6.setIsChange("Y");
        l6.setLocationname("Park Street");
        l7.setIsChange("Y");
        l7.setLocationname("Government Centre");
        l8.setIsChange("N");
        l8.setLocationname("Harvard");
        l9.setIsChange("N");
        l9.setLocationname("Chinatown");
        l10.setIsChange("Y");
        l10.setLocationname("Downtown Crossing");
        
        
        
        
        //v1
        LocationCatalog lc1 = new LocationCatalog();
        lc1.getLocationList().add(l1);
        lc1.getLocationList().add(l2);
        lc1.getLocationList().add(l3);
        lc1.getLocationList().add(l7);
        
        Vehicle v1= new Vehicle();
        v1.setPrice(10);
        v1.setVname("Blue");
        v1.setLc(lc1);
        
        
        //v2

        
        LocationCatalog lc2 = new LocationCatalog();
        lc2.getLocationList().add(l4);
        lc2.getLocationList().add(l5);
        lc2.getLocationList().add(l6);
        lc2.getLocationList().add(l7);
        
        Vehicle v2= new Vehicle();
        v2.setPrice(15);
        v2.setVname("Green");
        v2.setLc(lc2);
        
        //v3
        LocationCatalog lc3 = new LocationCatalog();
        lc3.getLocationList().add(l4);
        lc3.getLocationList().add(l1);
        lc3.getLocationList().add(l10);
        lc3.getLocationList().add(l9);
        
        Vehicle v3= new Vehicle();
        v3.setPrice(5);
        v3.setVname("Orange");
        v3.setLc(lc3);
        
        //v4
        LocationCatalog lc4 = new LocationCatalog();
        lc4.getLocationList().add(l6);
        lc4.getLocationList().add(l8);
        lc4.getLocationList().add(l10);
        
        Vehicle v4= new Vehicle();
        v4.setPrice(20);
        v4.setVname("Red");
        v4.setLc(lc4);
        
        VehicleCatalog vc1 = new VehicleCatalog();
        
        vc1.getVehicleList().add(v1);
        vc1.getVehicleList().add(v2);
        vc1.getVehicleList().add(v3);
        vc1.getVehicleList().add(v4);
        
        
        
        t1.setVehicleCatalog(vc1);
        
        
        
        
        
        
        Transport t2 = new Transport("Bus");
        
        Location la = new Location();
        Location lb = new Location();
        Location lc = new Location();
        Location ld = new Location();
        Location le = new Location();
        
        la.setIsChange("Y");
        la.setLocationname("Union Square");
        
        lc.setIsChange("N");
        lc.setLocationname("Watertown Yard");


        
        LocationCatalog lca = new LocationCatalog();
        lca.getLocationList().add(la);
        lca.getLocationList().add(l8);
        
        Vehicle va = new Vehicle();
        va.setLc(lca);
        va.setPrice(2);
        va.setVname("No.66");
        
        LocationCatalog lcb = new LocationCatalog();
        lcb.getLocationList().add(lc);
        lcb.getLocationList().add(la);
        
        Vehicle vb = new Vehicle();
        vb.setLc(lcb);
        vb.setPrice(3);
        vb.setVname("No.57");
        
        Transport t3 = new Transport("Taxi");
        
        
        VehicleCatalog vc2 = new VehicleCatalog();
        
        vc2.getVehicleList().add(va);
        vc2.getVehicleList().add(vb);

        t2.setVehicleCatalog(vc2);
        
        
        transportList.add(t1);
        transportList.add(t2);
        transportList.add(t3);
        
        
        
    }

    public ArrayList<Transport> getTransportList() {
        return transportList;
    }
    
    
    
}
