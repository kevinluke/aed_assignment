/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
        Organization o1 = new AdminOrganization();
        organizationList.add(o1);
        Organization o2 = new UserOrganization();
        organizationList.add(o2);
        Organization o3 = new TaxiOrganization();
        organizationList.add(o3);
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    

}
