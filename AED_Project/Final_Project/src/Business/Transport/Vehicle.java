/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Transport;

import Business.Location.LocationCatalog;

/**
 *
 * @author 健行
 */
public class Vehicle {
    
    private String vname;
    private float price;
    private LocationCatalog lc;

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public LocationCatalog getLc() {
        return lc;
    }

    public void setLc(LocationCatalog lc) {
        this.lc = lc;
    }

    @Override
    public String toString(){
        return vname;
    }
    
}
