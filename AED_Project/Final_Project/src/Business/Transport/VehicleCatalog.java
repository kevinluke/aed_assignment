/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Transport;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class VehicleCatalog {
    private ArrayList<Vehicle> VehiclenList;
    
    public VehicleCatalog(){
        this.VehiclenList = new ArrayList<>();
    }
    
    public Vehicle addVehicle(Vehicle v){
        VehiclenList.add(v);
        return v;
    }
    
    
    public void deleteVehicle(Vehicle vehicle){
        VehiclenList.remove(vehicle);
    }

    public ArrayList<Vehicle> getVehicleList() {
        return VehiclenList;
    }
        
}
