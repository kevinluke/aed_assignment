/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.User;

import java.util.ArrayList;

/**
 *
 * @author 健行
 */
public class UserDirectory {
    
    private ArrayList<User> UserList;
    
    public UserDirectory(){
        this.UserList = new ArrayList<>();
    }
    
    public User createUser(String name){
        User user = new User();
        user.setUsername(name);
        UserList.add(user);
        return user;
    }
    
    public void deleteUser(User user){
        UserList.remove(user);
    }

    public ArrayList<User> getUserList() {
        return UserList;
    }
    
    public User searchUser(String keyword){
        for (User user: UserList){
            if (user.getUsername().equals(keyword)){
                return user;
            }
        }
        return null;
    }
}
