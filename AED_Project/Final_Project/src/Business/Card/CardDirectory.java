/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Card;


import java.util.ArrayList;

/**
 *
 * @author 健行
 */

// CardDirectory class is used to store cards as an arraylist, which is utilized 
// in User
public class CardDirectory {
    private ArrayList<Card> CardList;
    
    public CardDirectory(){
        this.CardList = new ArrayList<>();
    }
    
    public Card addCard(Card card){
        CardList.add(card);
        return card;
    }
    
    public void deleteCard(Card card){
        CardList.remove(card);
    }

    public ArrayList<Card> getCardList() {
        return CardList;
    }
    

}
