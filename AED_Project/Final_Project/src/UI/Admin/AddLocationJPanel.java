/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.Admin;

import Business.Location.Location;
import Business.Transport.Transport;
import Business.Transport.Vehicle;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author 健行
 */
public class AddLocationJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Transport transport;
    private Vehicle vehicle;
    /**
     * Creates new form AddLocationPanel
     */
    public AddLocationJPanel(JPanel upc, Vehicle v,Transport t) {
        initComponents();
        userProcessContainer = upc;
        transport = t;
        vehicle = v;
        changeBox.removeAllItems();
        changeBox.setEditable(false);
        if (!vehicle.getLc().equals(null)){
            populateTable();
        }
        
    }
    
    public void populateBox(){
        
        if (yRBtn.isSelected()){
            changeBox.setEditable(true);
            
            for (Vehicle v : transport.getVehicleCatalog().getVehicleList()){
                if (!v.equals(vehicle)){
                    changeBox.addItem(v);
                }
            }
            
            
        }
        
        
    }

    public void populateTable(){
        DefaultTableModel model = (DefaultTableModel) vehicleTable.getModel();

        model.setRowCount(0);
        
        for (Location l : vehicle.getLc().getLocationList()){
            Object row[] = new Object[2];
            row[0] = l == null ? null : l;
            row[1] = l == null ? null : l.getIsChange();
            ((DefaultTableModel) vehicleTable.getModel()).addRow(row);
        }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        backJButton = new javax.swing.JButton();
        nameTxt = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        yRBtn = new javax.swing.JRadioButton();
        nRBtn = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        vehicleTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        changeBox = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel1.setText("Location Name:");

        backJButton.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        backJButton.setText("<< Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        nameTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        nameTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTxtActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel2.setText("Change or not:");

        buttonGroup1.add(yRBtn);
        yRBtn.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        yRBtn.setText("Yes");
        yRBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yRBtnActionPerformed(evt);
            }
        });

        buttonGroup1.add(nRBtn);
        nRBtn.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        nRBtn.setText("No");
        nRBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nRBtnActionPerformed(evt);
            }
        });

        vehicleTable.setFont(new java.awt.Font("Lucida Sans Unicode", 1, 14)); // NOI18N
        vehicleTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Location Name", "Change or not"
            }
        ));
        jScrollPane1.setViewportView(vehicleTable);

        jLabel3.setFont(new java.awt.Font("Showcard Gothic", 1, 36)); // NOI18N
        jLabel3.setText("Vehicle Information:");

        jButton1.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton1.setText("Add Location");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        changeBox.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        changeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        changeBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeBoxActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel4.setText("Change to:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(111, 111, 111)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(backJButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(442, 442, 442)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(179, 179, 179)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(53, 53, 53)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(nRBtn)
                                    .addComponent(nameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4))
                                .addGap(53, 53, 53)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(changeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(yRBtn)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jLabel3)))
                .addContainerGap(135, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(nameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(yRBtn)
                    .addComponent(nRBtn))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(changeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backJButton)
                    .addComponent(jButton1))
                .addGap(70, 70, 70))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if (!yRBtn.isSelected()){
            if (!nameTxt.getText().equals("")){
                Location l = new Location();
                l.setLocationname(nameTxt.getText());
                l.setIsChange("N");
                vehicle.getLc().addLocation(l);
                JOptionPane.showMessageDialog(null, "Add Successfully!");
                populateTable();
            }
            else {
                JOptionPane.showMessageDialog(null, "Please enter the name first!");
            }
        }
        else {
            if (!nameTxt.getText().equals("")){
                Vehicle v = (Vehicle) changeBox.getSelectedItem();
                    
                Location l = new Location();
                Location lto = new Location();
                l.setLocationname(nameTxt.getText());
                l.setIsChange("Y");
                lto.setLocationname(nameTxt.getText());
                lto.setIsChange("Y");
                vehicle.getLc().addLocation(l);
                v.getLc().addLocation(lto);
                JOptionPane.showMessageDialog(null, "Add Successfully!");
                populateTable();
            }
            else {
                JOptionPane.showMessageDialog(null, "Please enter the name first!");
            }
        }
            
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void nameTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTxtActionPerformed

    private void changeBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_changeBoxActionPerformed

    private void yRBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yRBtnActionPerformed

        populateBox();// TODO add your handling code here:
    }//GEN-LAST:event_yRBtnActionPerformed

    private void nRBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nRBtnActionPerformed

        changeBox.removeAllItems();
        changeBox.setEditable(false);

// TODO add your handling code here:
    }//GEN-LAST:event_nRBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox changeBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton nRBtn;
    private javax.swing.JTextField nameTxt;
    private javax.swing.JTable vehicleTable;
    private javax.swing.JRadioButton yRBtn;
    // End of variables declaration//GEN-END:variables
}
