/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.User;

import Business.Card.Card;
import Business.Enterprise.TransportEnterprise;
import Business.Organization.AdminOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.RechargeWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author 健行
 */

public class RechargeJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount account;
    private TransportEnterprise business;
    /**
     * Creates new form RechargeJPanel
     */
    public RechargeJPanel(JPanel upc, UserAccount ua, TransportEnterprise b) {
        initComponents();
        userProcessContainer = upc;
        account = ua;
        business = b;
        populateTable();
        populateTaxi();
        statusTxt.setEditable(false);
        moneyTxt.setEditable(false);
    }

    public void populateTaxi(){
        if (account.getWorkQueue().getWorkRequestList().size()>=1){
            for (WorkRequest request1 : account.getWorkQueue().getWorkRequestList()){
                if (request1.getType().equals("Recharge")){
                    if(request1.getStatus().equals("Y")){
                        statusTxt.setText("Accepted");
                        moneyTxt.setEditable(false);
                        moneyTxt.setText(request1.getMessage());
                    }                
                    else if(request1.getStatus().equals("Sent")){
                        statusTxt.setText("Sent");
                        moneyTxt.setText(request1.getMessage());
                    }
                    else if(request1.getStatus().equals("Pending")){
                        statusTxt.setText("Pending");
                        moneyTxt.setText(request1.getMessage());
                    }
                    else{
                        statusTxt.setText("Denied");
                        moneyTxt.setEditable(true);
                    }    
                }

            }
                  
        }
    }

    
    private void populateTable(){
        
        DefaultTableModel model = (DefaultTableModel) cardTable.getModel();

        model.setRowCount(0);

        for (Card card : account.getUser().getCardDirectory().getCardList()) {
            Object[] row = new Object[4];
            row[0] = card.getCardid();
            row[1] = card;
            row[2] = card.getExpireDate();
            row[3] = card.getBalance();

            model.addRow(row);
        }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        cardTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        backJButton = new javax.swing.JButton();
        moneyTxt = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        statusTxt = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        EBtn = new javax.swing.JRadioButton();
        BBtn = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        weekBtn = new javax.swing.JRadioButton();
        monthBtn = new javax.swing.JRadioButton();
        yearBtn = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        balanceTxt = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        chargeTxt = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();

        jLabel7.setText("jLabel7");

        setMaximumSize(new java.awt.Dimension(700, 500));
        setMinimumSize(new java.awt.Dimension(700, 500));
        setName(""); // NOI18N

        cardTable.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        cardTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Card id", "Type", "Expire Date", "Balance"
            }
        ));
        jScrollPane1.setViewportView(cardTable);

        jLabel1.setFont(new java.awt.Font("Showcard Gothic", 1, 36)); // NOI18N
        jLabel1.setText("Card Information:");

        backJButton.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        backJButton.setText("<< Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        moneyTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel2.setText("Money:");

        jLabel3.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel3.setText("Status:");

        statusTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jButton1.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton2.setText("Confirm");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel4.setText("Recharge:");

        jButton3.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton3.setText("Add new card");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel5.setText("Add new Card:");

        buttonGroup1.add(EBtn);
        EBtn.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        EBtn.setText("Expired");

        buttonGroup1.add(BBtn);
        BBtn.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        BBtn.setText("Balance");

        jLabel6.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel6.setText("Active:");

        buttonGroup2.add(weekBtn);
        weekBtn.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        weekBtn.setText("One week $50");
        weekBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                weekBtnActionPerformed(evt);
            }
        });

        buttonGroup2.add(monthBtn);
        monthBtn.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        monthBtn.setText("One month $ 150");

        buttonGroup2.add(yearBtn);
        yearBtn.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        yearBtn.setText("One year $1500");

        jLabel8.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel8.setText("Money for Balance Card:");

        balanceTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jLabel9.setText("Balance:");

        chargeTxt.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N

        jButton4.setFont(new java.awt.Font("Eras Light ITC", 1, 14)); // NOI18N
        jButton4.setText("Delete");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(122, 122, 122)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(backJButton)
                                .addGap(9, 9, 9)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(18, 18, 18)
                                        .addComponent(EBtn)
                                        .addGap(18, 18, 18)
                                        .addComponent(BBtn)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton3))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addGap(18, 18, 18)
                                        .addComponent(balanceTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(weekBtn)
                                        .addGap(12, 12, 12)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButton2)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(monthBtn)
                                                .addGap(18, 18, 18)
                                                .addComponent(yearBtn))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel2)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(chargeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel9)
                                                    .addGap(10, 10, 10)
                                                    .addComponent(moneyTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(statusTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButton4)
                                            .addComponent(jButton1)))))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)))
                .addGap(49, 66, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2)
                    .addComponent(chargeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(statusTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(moneyTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(balanceTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(EBtn)
                    .addComponent(BBtn)
                    .addComponent(jButton3))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(weekBtn)
                    .addComponent(monthBtn)
                    .addComponent(yearBtn))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backJButton)
                    .addComponent(jButton2))
                .addContainerGap(30, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        String message = chargeTxt.getText();

        if(message.equals("") || message.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please enter price.");
            return;
        }
        RechargeWorkRequest request = new RechargeWorkRequest();
        request.setMessage(message);
        request.setSender(account);
        request.setStatus("Sent");
        request.setType("Recharge");
        
        Organization org = null;
        for (Organization organization : business.getOrganizationDirectory().getOrganizationList()){
            if (organization instanceof AdminOrganization){
                org = organization;
                break;
            }
        }
        if (org!=null){
            org.getWorkQueue().getWorkRequestList().add(request);
            if (account.getWorkQueue().getWorkRequestList().size()<1){
                account.getWorkQueue().getWorkRequestList().add(request);
            }
            else{
                for (int i =0 ; i < account.getWorkQueue().getWorkRequestList().size() ; i++){
                    WorkRequest wr = account.getWorkQueue().getWorkRequestList().get(i);
                    if (wr.getType().equals("Recharge")){
                        account.getWorkQueue().getWorkRequestList().remove(wr);
                        account.getWorkQueue().getWorkRequestList().add(request);
                    }
                    else {
                        account.getWorkQueue().getWorkRequestList().add(request);
                    }
                }
            }

        }
        
        
        JOptionPane.showMessageDialog(null, "Request message sent");
        
        for (WorkRequest request1 : account.getWorkQueue().getWorkRequestList()){
            statusTxt.setText(request1.getStatus());
            System.out.print("333");
        }
        chargeTxt.setText("");
        System.out.print(account.getWorkQueue().getWorkRequestList().size());
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        int selectedRow = cardTable.getSelectedRow();

        if (selectedRow >= 0) {
            Card card = (Card) cardTable.getValueAt(selectedRow, 1);
            if (statusTxt.getText().equals("Accepted")){
                Float balance = Float.parseFloat(moneyTxt.getText());
                if (card.getCardType().equals("Money")){
                    balanceTxt.setEditable(true);
                    if (Float.parseFloat(balanceTxt.getText()) <= balance){
                        card.setBalance(card.getBalance()+Float.parseFloat(balanceTxt.getText()));
                        JOptionPane.showMessageDialog(null, "Congratulation, the balance has become "+ card.getBalance());
                        moneyTxt.setText(String.valueOf(balance - Float.parseFloat(balanceTxt.getText())));
                        populateTable();
                        account.getWorkQueue().getWorkRequestList().get(0).setMessage(moneyTxt.getText());
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "You do not have enough money!");
                    } 
                }
                else{
                    if (weekBtn.isSelected()){
                        if (balance >= 50){
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
                            Date date = null;
                            try{
                                date = sdf.parse(card.getExpireDate());
                            }
                            catch (ParseException e){
                                e.printStackTrace(); 
                            }
                            Calendar cl = Calendar.getInstance();  
                            cl.setTime(date);
                            cl.add(Calendar.DATE, +7);
                            date = cl.getTime();
                            card.setExpireDate(sdf.format(date));
                            JOptionPane.showMessageDialog(null, "Congratulation, the expired date has become "+ card.getExpireDate());
                            moneyTxt.setText(String.valueOf(balance - 50));
                            populateTable();
                            account.getWorkQueue().getWorkRequestList().get(0).setMessage(moneyTxt.getText());
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "You do not have enough money!");
                        }    
                    }
                    else if (monthBtn.isSelected()){
                        if (balance >= 150){
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
                            Date date = null;
                            try{
                                date = sdf.parse(card.getExpireDate());
                            }
                            catch (ParseException e){
                                e.printStackTrace(); 
                            }
                            Calendar cl = Calendar.getInstance();  
                            cl.setTime(date);
                            cl.add(Calendar.MONTH, +1);
                            date = cl.getTime();
                            card.setExpireDate(sdf.format(date));
                            JOptionPane.showMessageDialog(null, "Congratulation, the expired date has become "+ card.getExpireDate());
                            moneyTxt.setText(String.valueOf(balance - 150));
                            populateTable();
                            account.getWorkQueue().getWorkRequestList().get(0).setMessage(moneyTxt.getText());
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "You do not have enough money!");
                        }
                    }
                    else {
                        if (balance >= 1500){
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
                            Date date = null;
                            try{
                                date = sdf.parse(card.getExpireDate());
                            }
                            catch (ParseException e){
                                e.printStackTrace(); 
                            }
                            Calendar cl = Calendar.getInstance();  
                            cl.setTime(date);
                            cl.add(Calendar.YEAR, +1);
                            date = cl.getTime();
                            card.setExpireDate(sdf.format(date));
                            JOptionPane.showMessageDialog(null, "Congratulation, the expired date has become "+ card.getExpireDate());
                            moneyTxt.setText(String.valueOf(balance - 1500));
                            populateTable();
                            account.getWorkQueue().getWorkRequestList().get(0).setMessage(moneyTxt.getText());
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "You do not have enough money!");
                        }
                    }
                }
                    
            }
            else {
                JOptionPane.showMessageDialog(null, "Wrong price, please enter again!");
            }
            
        }
        else {
            JOptionPane.showMessageDialog(null, "Choose a request to process.");
            return;
        }
        
       
// TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        Card card = new Card();
        card.setCardid(account.getUser().getCardDirectory().getCardList().size()+1);
        if (BBtn.isSelected()){
            card.setCardType("Money");
            card.setExpireDate("9999-12-31");
            card.setBalance(0);
            account.getUser().getCardDirectory().addCard(card);
        }
        else if (EBtn.isSelected()){
            card.setCardType("Expire");
            card.setBalance(0);
            Date date = new Date();
            String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(date);
            card.setExpireDate(dateStr);
            account.getUser().getCardDirectory().addCard(card);
        }
        else {
            JOptionPane.showMessageDialog(null, "Please choose first!");
        }
        populateTable();
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void weekBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_weekBtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_weekBtnActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        int selectedRow = cardTable.getSelectedRow();

        if (selectedRow >= 0) {
            Card card = (Card) cardTable.getValueAt(selectedRow, 1);
            if (card.getCardType().equals("Money")){
                if (card.getBalance() > 0){
                    int n = JOptionPane.showConfirmDialog(null, "Balance is "+ card.getBalance() +"! Are you sure to delete this card ?", "Notice",JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION){
                        account.getUser().getCardDirectory().deleteCard(card);
                        populateTable();
                    }
                    else {
                        return;
                    }
                }
                else{
                    int n = JOptionPane.showConfirmDialog(null, "Are you sure to delete this card ?", "Notice",JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION){
                        account.getUser().getCardDirectory().deleteCard(card);
                        populateTable();
                    }
                    else {
                        return;
                    }
                }
            }
            else {
                Calendar cl = Calendar.getInstance();
                Date date = new Date();
                Date d = null;
                cl.setTime(date);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try{
                    d = sdf.parse(card.getExpireDate());
                }
                catch(ParseException e){
                    e.printStackTrace();
                }
                if (cl.before(d)){
                    int n = JOptionPane.showConfirmDialog(null, "This card is expired on "+ card.getExpireDate() +"! Are you sure to delete this card ?", "Notice",JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION){
                        account.getUser().getCardDirectory().deleteCard(card);
                        populateTable();
                    }
                    else {
                        return;
                    }
                }
                else{
                    int n = JOptionPane.showConfirmDialog(null, "Are you sure to delete this card ?", "Notice",JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION){
                        account.getUser().getCardDirectory().deleteCard(card);
                        populateTable();
                    }
                    else {
                        return;
                    }
                }
            }
        
        }
        else {
            JOptionPane.showMessageDialog(null, "Please choose one card!");
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton BBtn;
    private javax.swing.JRadioButton EBtn;
    private javax.swing.JButton backJButton;
    private javax.swing.JTextField balanceTxt;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JTable cardTable;
    private javax.swing.JTextField chargeTxt;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField moneyTxt;
    private javax.swing.JRadioButton monthBtn;
    private javax.swing.JTextField statusTxt;
    private javax.swing.JRadioButton weekBtn;
    private javax.swing.JRadioButton yearBtn;
    // End of variables declaration//GEN-END:variables
}
